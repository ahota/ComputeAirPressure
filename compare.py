# Copyright (c) 2017, Sandia National Laboratories.  All rights
# reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Author: Ben Newton
# Script to determine vectorization performance improvement with various
#  block sizes compared to the original vtk-m on various machines

#example usage on hansen:
# python compare.py

#example usage on bowman (-k to keep builds (not delete) after done):
# python compare.py -k

#example usage to plot bowman results on another machine:
# python compare.py -p -r /home/bdnewto/research/paraview/ComputeAirPressure/results_bowman-lsm1_2017-08-07_13-19-28-novecsCross-1-0 -o bowman -t knl-alpha

#python compare.py -p -r /home/bdnewto/research/paraview/ComputeAirPressure/results_hansen01_2017-08-14_11-39-11 -o hansen01 -t hansen

#compare 2 results
# python compare.py -p -r /home/bdnewto/research/paraview/ComputeAirPressure/results_bowman-lsm1_2017-08-07_13-19-28 -s /home/bdnewto/research/paraview/ComputeAirPressure/results_bowman-lsm1_2017-08-07_13-20-28 -o bowman -t knl-alpha

# This is a script thrown together to allow for comparison of vtkm with and
# without vectoriztion on multiple platfroms and with multiple block sizes.

# currently bowman and hansen are supported.  Others could be added.

# the list of blockSizes list can be adjusted to test different block sizes.

#create a "root" directory for this project, and clone the ComputeAirPressure,
#CrossProduct, DotProduct repos into that directory.  Also clone a version of
#vtkm without vectorization and a verstion of vtkm with vectorization into
# that directory.  Adjust the 3 paths below accordingly.

doPlot = True

root = "/home/bdnewto/research/paraview"
pathToOriginalVtkm=root+"/vtk-m-master"
pathToNewVtkm=root+"/vtk-m"

#also, you may want to add your host machine name here, if you plant to run on
# it (in addtion to hansen and bowman, etc)
my_hostname = "finglas.srn.sandia.gov"

import os
from subprocess import call
import socket
import time

import csv
import os
import socket

import subprocess

from optparse import OptionParser

def readData(path):
    d = []
    try:
        with open(path) as file:
            reader = csv.reader(file, delimiter=',')
            for row in reader:
                d.append(int(row[0])/1000) #one item per row #convert to ms
    except IOError as e:
        print("Error- File not found - "+path+" - filling with 0's instead")
        for i in range(options.numTrials):
            d.append(0) #or some other value
    return d
#could use np.genfromtxt later instead?? todo

#plot the results
def plot(resultsDir, secondResultsDir):
    os.chdir(resultsDir)
    figNum=1
    for appPath in applicationPaths:
        data = []
        for blkSz in blockSizes:
            data.append(readData(resultsDir+"/times_"+str(options.dataLength)+"_compare_"+partition+"_"+str(blkSz)+"_"+appPath[1]+".csv"))

        mean=[sum(row)/float(len(row)) for row in data]

        if secondResultsDir: # if not empty
            data2 = []
            for blkSz in blockSizes:
                data2.append(readData(secondResultsDir+"/times_"+str(options.dataLength)+"_compare_"+partition+"_"+str(blkSz)+"_"+appPath[1]+".csv")) #at some point may need to provide options to compare different partitions

            mean2=[sum(row)/float(len(row)) for row in data2]

        plt.figure(figNum, figsize=(8,6))
        figNum+=1

        #plot original mean line
        orig=readData(resultsDir+"/times_"+str(options.dataLength)+"_compare_original_"+appPath[1]+".csv")
        original = sum(orig)/float(len(orig))
        plt.axhline(y=original, color='green', linestyle='--', label="original code")
        plt.boxplot(orig, sym='')#showfliers=False)
        x=np.random.normal(1,0.09,size=len(orig))
        plt.plot(x,orig, marker='o', markerfacecolor='none', markeredgecolor="green", linestyle="", markersize=3, alpha=0.3)

        #plot fastest mean line
        if appPath[2] != None:
            origf=readData(resultsDir+"/times_"+str(options.dataLength)+"_fastest_"+appPath[2]+".csv")
            fastest = sum(origf)/float(len(origf))
            plt.axhline(y=fastest, color='magenta', linestyle=':', label="optimal")
            plt.boxplot(origf, sym='', positions=[2])
            x=np.random.normal(2,0.09,size=len(origf))
            plt.plot(x,origf,marker='o', markerfacecolor='none', markeredgecolor="magenta", linestyle="", markersize=3, alpha=0.3)

        #plot box plots and mean for all other data
        plt.boxplot(data, sym='')#showfliers=False)
        plt.plot(range(1,len(blockSizes)+1), mean, color="blue", label="with vectorization") #plot the mean (with changes) line

        #plot scatter
        for i in range(len(blockSizes)):
            x=np.random.normal(i+1,0.09,size=len(data[i]))
            plt.plot(x,data[i], marker='o', markerfacecolor='none', markeredgecolor="blue", linestyle="", markersize=3, alpha=0.3)

        #plot second for comparison (if desired)  #should make into a function (same as above) todo
        if secondResultsDir:
            plt.boxplot(data2, sym='')
            plt.plot(range(1,len(blockSizes)+1), mean2, color="orange", label="previous with vectorization") #plot the mean (with changes) line

            #plot scatter second
            for i in range(len(blockSizes)):
                x=np.random.normal(i+1,0.09,size=len(data2[i]))
                plt.plot(x,data2[i], marker='o', markerfacecolor='none', markeredgecolor="orange", linestyle="", markersize=3, alpha=0.3)

        ax=plt.gca()

        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels)

        ax.set_ylim([0,800])
        ax.set_xticklabels(blockSizes)
        ax.set_xlabel("block size")
        ax.set_ylabel("run time (ms)")
        ax.set_title(appPath[1]+" - "+host+" - "+partition)
        ax2 = ax.twinx()
        #add tick at fastest
        ax2.set_ylim(ax.get_ylim())
        ax2.set_yticks([fastest, original, min(mean)])

        #plt.gca().yaxis.set_major_locator(MaxNLocator(prune='lower'))   #was trying to remove overlapping yticks.

        plt.savefig(appPath[1]+"_"+host+"_"+partition+".png")
    #endfor

#below is list of triplets of path and executable name and which fastest executable name this one maps to
applicationPaths=[#[root+"/ComputeAirPressure", "ComputeAirPressure_SERIAL", "ComputeAirPressureFastest_SERIAL"],
                  [root+"/ComputeAirPressure", "ComputeAirPressure64_SERIAL", "ComputeAirPressureFastest64_SERIAL"],
                  [root+"/CrossProduct", "CrossProduct_SERIAL", "CrossProductFastest_SERIAL"],
                  [root+"/CrossProduct", "CrossProductNoVecs_SERIAL", "CrossProductFastest_SERIAL"],
                  [root+"/CrossProduct", "CrossProductReturnValue_SERIAL", "CrossProductFastest_SERIAL"],
                  [root+"/DotProduct", "DotProduct_SERIAL", "DotProductFastest_SERIAL"],
                  [root+"/DotProduct", "VectorNorm_SERIAL", "VectorNormFastest_SERIAL"],
                  [root+"/DotProduct", "MarchingCubes_SERIAL", None],
                  [root+"/DotProduct", "MyVectorMagnitude_SERIAL", None],
                  [root+"/DotProduct", "VectorNormReturnValue_SERIAL", "VectorNormFastest_SERIAL"],
                  [root+"/DotProduct", "DotProductReturnValue_SERIAL", "DotProductFastest_SERIAL"],
                  [root+"/DotProduct", "DotProductNoVecs_SERIAL", "DotProductFastest_SERIAL"]]

fastestApplicationPaths=[[root+"/ComputeAirPressure", "ComputeAirPressureFastest_SERIAL"],
                         [root+"/ComputeAirPressure", "ComputeAirPressureFastest64_SERIAL"],
                         [root+"/CrossProduct", "CrossProductFastest_SERIAL"],
                         [root+"/DotProduct", "DotProductFastest_SERIAL"],
                         [root+"/DotProduct", "VectorNormFastest_SERIAL"]]

hostDefault=socket.gethostname()  #use instead hostname --short to get just finglas?
timestr = time.strftime("%Y-%m-%d_%H-%M-%S")
resultsDirDefault = applicationPaths[0][0]+"/results_"+hostDefault+"_"+timestr
secondResultsDirDefault=""
appendStringDefault=""

parser = OptionParser()
parser.add_option("-n", "--numTrials", dest="numTrials", help="The number or trials to run in each configuration [default=20]", metavar="NUM", default=20)
parser.add_option("-l", "--length", dest="dataLength", help="The length or size of the data [default=256]", metavar="NUM", default=256)
parser.add_option("-r", "--resultsDir", dest="resultsDir", help="The full path to the directory to read and/or write results data to", metavar="FILE", default=resultsDirDefault)
parser.add_option("-p", "--plotOnly", action="store_true", dest="plotOnly", help="Don't run tests, only plot the data from the given results directory (-r), hostname (-o), and partition (-t)")
parser.add_option("-o", "--hostname", dest="host", help="When plotting only this gives the name of host results were obtained on", default=hostDefault)
parser.add_option("-t", "--partition", dest="partition", help="(optional) When only plotting this gives the name of the partition results were obtained on (i.e. knl-alpha) (does not apply to all machines)", default="knl-alpha")
parser.add_option("-k", "--keepBuilds", action="store_true", dest="keepBuilds", help="flag that indicates the build directories should not be removed upon completion")
parser.add_option("-s", "--secondResultsDir", dest="secondResultsDir", help="Plot these results as well in plot only mode", metavar="FILE", default=secondResultsDirDefault)
parser.add_option("-a", "--appendString", dest="appendString", help="Append this string to the results directory name", default=appendStringDefault)

(options, args) = parser.parse_args()

numTrials = options.numTrials
dataLength = options.dataLength
if options.appendString:
    resultsDir = options.resultsDir+"_"+options.appendString
else:
    resultsDir = options.resultsDir
host = options.host
partition = options.partition
srun = True

try:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
except ImportError:
    doPlot = False
    print("Warning: Libraries needed to plot are not installed")

bowmanPartition=options.partition
#"knl-alpha" # others are delata, etc

blockSizes=[2**j for j in range(4,14)] #4 to start at 16 change to 0 to start at 1
#blockSizes.append(700) #optionally append these other sizes.
#blockSizes.append(3568)

vectorization=""
report="-fopt-info-vec-all"

print("Host=", host)

if host==my_hostname:
    srun=False

if host=="bowman-lsm1":
    vectorization="knl" #aka avx512
    partition=bowmanPartition
    report = "-qopt-report=5" #print optimization report

if host=="hansen01":
    vectorization="avx2"
    partition="hansen"
    report = "-qopt-report=5" #todo should at some point have an option for
    #                       compiler (c++ or intel).  For now hansen is intel.

if not options.plotOnly:
    print("Executing module commands...")
    if host=="bowman-lsm1":
        #load intel compilers 17.1.132 only on bowman
        cmd=os.popen('/usr/bin/modulecmd python load intel/compilers/17.1.132')
        exec(cmd)

    if host=="hansen01":
        #set cmake module version 3.9.0 and compiler GCC 7.2.0
        cmd=os.popen('/usr/bin/modulecmd python load cmake/3.9.0') #changed from 3.4.3
        exec(cmd)
        #cmd=os.popen('/usr/bin/modulecmd python load gcc/7.2.0')  #changed from 5.2.0
        cmd=os.popen('/usr/bin/modulecmd python load intel/compilers/18.1.163')  #changed to intel?
        exec(cmd)
    print("Done executing module commands")

    start_time = time.time()

    #erase everything first!!! (in case not removed on last run i.e. ctrl-c) #should be a function todo unless keep
    if not options.keepBuilds:
        print("Erasing any previous data")
        for blkSz in blockSizes:
            call(["rm", "-rf", pathToNewVtkm+"/compare-"+str(blkSz)+"-build"])
            for appPath in applicationPaths:
                call(["rm", "-rf", appPath[0]+"/compare-"+str(blkSz)+"-build"])
        call(["rm", "-rf", pathToOriginalVtkm+"/compare-orig-build"])
        for appPath in applicationPaths:
            call(["rm", "-rf", appPath[0]+"/compare-orig-build"])
        os.chdir(applicationPaths[0][0])
        call(["rm", "compare.sh"])
        print("Done erasing any previous data")

    #make script to assist in running comparisons (delete later)
    #had to use this script since I wasn't able to get call
    #(["srun"... to work right with the args to the command being run.
    os.chdir(applicationPaths[0][0])
    file=open("compare.sh","w")
    file.write("#!/bin/bash\n")
    if srun:
        file.write("srun -p "+partition+" -N 1 --time=00:05:00 $1 -n $2 -l $3 -s $4 -c\n")
    else:
        file.write("$1 -n $2 -l $3 -s $4 -c\n")
    file.close()
    call(["chmod", "u+x", "compare.sh"])

    call(["mkdir", resultsDir])

    print("Compiling original version of vtk-m for comparison ...")
    #compile original version of vtkm library
    os.chdir(pathToOriginalVtkm)
    call(["mkdir", "compare-orig-build"])
    os.chdir("compare-orig-build")
    call(["cmake", "-DCMAKE_BUILD_TYPE=Release", "-DVTKm_ENABLE_RENDERING=OFF", "-DVTKm_ENABLE_TESTING=OFF", "-DVTKm_Vectorization="+vectorization, "../"])
    call(["make", "-j"])
    print("Done compiling original versin of vtk-m")

    print("Compiling and running benchmark applications with the original version of vtk-m for comparison ...")
    #compile apps with original verstion of vtkm
    for appPath in applicationPaths:
        os.chdir(appPath[0])
        call(["mkdir", "compare-orig-build"])
        os.chdir("compare-orig-build")
        call(["cmake", "-DCMAKE_BUILD_TYPE=Release", "-DVTKm_DIR="+pathToOriginalVtkm+"/compare-orig-build/lib/cmake/vtkm-1.1/", "-DVTKm_Vectorization="+vectorization, "-DCMAKE_CXX_FLAGS="+report, "../"])
        call(["make", "-j"])

        #run apps with original verstion of the vtkm library
        os.chdir(resultsDir)#so .csv files end up here.
    for appPath in applicationPaths:
        call([applicationPaths[0][0]+"/compare.sh", appPath[0]+"/compare-orig-build/"+appPath[1], str(options.numTrials), str(options.dataLength), "compare_original_"+appPath[1]])
    print("Done compiling and runningbenchmark applications with the original version of vtk-m.")

    #run fastest apps (apps which don't use vtkm, and are assumed to be the
    # fastest possible way to obtain the same results)
    for appPath in fastestApplicationPaths:
        call([applicationPaths[0][0]+"/compare.sh", appPath[0]+"/compare-orig-build/"+appPath[1], str(options.numTrials), str(options.dataLength), "fastest_"+appPath[1]])

    for blkSz in blockSizes:
        print("Compiling vectorized vtk-m for the "+str(blkSz)+ " block size ...")
        #compile new (vectorized) vtkm libaray
        os.chdir(pathToNewVtkm)
        call(["mkdir", "compare-"+str(blkSz)+"-build"])
        os.chdir("compare-"+str(blkSz)+"-build")
        call(["cmake", "-DCMAKE_BUILD_TYPE=Release",  "-DVTKm_BLOCK_SIZE="+str(blkSz), "-DVTKm_ENABLE_RENDERING=OFF", "-DVTKm_ENABLE_TESTING=OFF", "-DVTKm_Vectorization="+vectorization, "../"]) #without report
        call(["make", "-j"])
        print("Done compiling vectorized vtk-m for the "+str(blkSz)+ " block size.")

        print("Compiling and running benchmark applicaitons for vectorized vtk-m for the "+str(blkSz)+ " block size...")
        #compile the apps with the vectorized version of vtkm
        for appPath in applicationPaths:
            os.chdir(appPath[0])
            call(["mkdir", "compare-"+str(blkSz)+"-build"])
            os.chdir("compare-"+str(blkSz)+"-build")
            if blkSz == 512:
                call(["cmake","-DCMAKE_BUILD_TYPE=Release", "-DVTKm_DIR="+pathToNewVtkm+"/compare-"+str(blkSz)+"-build/lib/cmake/vtkm-1.1", "-DVTKm_Vectorization="+vectorization, "-DCMAKE_CXX_FLAGS="+report, "../"]) #with report
            else:
                call(["cmake","-DCMAKE_BUILD_TYPE=Release", "-DVTKm_DIR="+pathToNewVtkm+"/compare-"+str(blkSz)+"-build/lib/cmake/vtkm-1.1", "-DVTKm_Vectorization="+vectorization, "../"]) #no report
            call(["make", "-j"])

        os.chdir(resultsDir)#so .csv files end up here.
        #run apps with new vectorized vtkm library
        for appPath in applicationPaths:
            call([applicationPaths[0][0]+"/compare.sh", appPath[0]+"/compare-"+str(blkSz)+"-build/"+appPath[1], str(options.numTrials), str(options.dataLength), "compare_"+partition+"_"+str(blkSz)+"_"+appPath[1]])
        print("Done compiling and running benchmark applicaitons for vectorized vtk-m for the "+str(blkSz)+ " block size...")

        if not options.keepBuilds:
            call(["rm", "-rf", pathToNewVtkm+"/compare-"+str(blkSz)+"-build"])
            for appPath in applicationPaths:
                call(["rm", "-rf", appPath[0]+"/compare-"+str(blkSz)+"-build"])
    if not options.keepBuilds:
        call(["rm", "-rf", pathToOriginalVtkm+"/compare-orig-build"])
        for appPath in applicationPaths:
            call(["rm", "-rf", appPath[0]+"/compare-orig-build"])
    #remove compare.sh file generated above
    os.chdir(applicationPaths[0][0])
    call(["rm", "compare.sh"])
    ####call(["rm", "results/times_*_compare_*.csv"] #remove hopefully only the generated csv files #comment this line to keep .csv files
    #call(["rm", "-rf", "results"]) #remove results #comment this line to keep csv files and results directory.

    print("Done comparing. Elapsed Time: %s seconds" % (time.time() - start_time))
    print("results in "+resultsDir)
#endif

if doPlot:
    plot(resultsDir, options.secondResultsDir)
#endif
else:
    print("Not plotting")

if not options.plotOnly:
    #print these instructions for copying the files to your machine and/or
    # generating plots
    resultsDirOnly = os.path.basename(resultsDir)
    print("  To plot run the following on this machine:")
    print("cd "+applicationPaths[0][0]+"; tar -zcvf res.tar.gz "+resultsDirOnly)
    print("  and the following on a machine with matplotlib installed")
    #output = subprocess.check_output("hostname --ip-address", shell=True)
    if host=="bowman-lsm1": #figure out how to convert hostname to scp hostname below?
        print("cd "+applicationPaths[0][0]+"; scp bowman:"+applicationPaths[0][0]+"/res.tar.gz .; tar -zxvf res.tar.gz; rm res.tar.gz; python compare.py -p -r "+resultsDir+" -o "+host+" -t "+partition+"; cd "+resultsDir+"; nautilus .")
    elif host=="hansen01":
        print("cd "+applicationPaths[0][0]+"; scp hansen:"+applicationPaths[0][0]+"/res.tar.gz .; tar -zxvf res.tar.gz; rm res.tar.gz; python compare.py -p -r "+resultsDir+" -o "+host+" -t "+partition+"; cd "+resultsDir+"; nautilus .")
    else:
        print("cd "+applicationPaths[0][0]+"; scp <hostname>:"+applicationPaths[0][0]+"/res.tar.gz .; tar -zxvf res.tar.gz; rm res.tar.gz; python compare.py -p -r "+resultsDir+" -o "+host+" -t "+partition+"; cd "+resultsDir+"; nautilus .")
    
