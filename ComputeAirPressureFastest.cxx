#include <iostream>
#include <chrono>
#include <string.h>
#include <iomanip>
#include <fstream>
#include <numeric>
#include <math.h>

int numIterations = 1;
//bool writeToFile = false,
bool saveTimes = false, checkAnswer = false;
int SIDE_LENGTH = 256;
std::string fileSuffix;

void cap(float* ax, float* ay, float* az,
         float lowz, float highz, float rangeLow, float rangeHigh, float* r, int len)
{
  float den = highz - lowz;
  for (int i = 0; i <len; ++i)
    {
    r[i] = ((rangeLow*(highz-az[i])) + (rangeHigh*(az[i]-lowz)))/den;
    }
}

void populateWithUnits(float* x, float* y, float* z,
                       int sideLen)
{
  int len = sideLen*sideLen*sideLen;
  for(int index = 0; index < len; index++)
  {
  x[index] = index % sideLen;
  y[index] = (index / sideLen) % sideLen;
  z[index] = ((index / sideLen / sideLen) % sideLen) * (2000.0/float(SIDE_LENGTH)); //moves values into range 0 to 2000 meters
  }
}

float lerp(float v0, float v1, float t) {
  return ((1 - t) * v0) + (t * v1);
}

void printUsage()
{
  std::cerr << "Usage: ComputeAirPressureFastest ["; //-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  for(int argi = 1; argi < argc; argi++) {
  /*if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
      }
      else*/
  if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
  else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Compute Air Pressure Fastest" << std::endl;

  const int ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;

  float* ax = new float[ARRAY_LENGTH];  //seems to be .021ms faster (for length 60^3) from the stack (float a[ARRAY_LENGTH]) but this means -l can be at most 60
  float* ay = new float[ARRAY_LENGTH];
  float* az = new float[ARRAY_LENGTH];
  float* r = new float[ARRAY_LENGTH];

  populateWithUnits(ax,ay,az, SIDE_LENGTH); //ARRAY_LENGTH);

  int total = 0;
  int times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(int index = 0; index < numIterations; index++)
    {
    if(index > 0)
      std::cout << "\r";
    auto st = std::chrono::high_resolution_clock::now();
    cap(ax, ay, az, 0.0, 2000.0, 101325.0, 77325.0, r, ARRAY_LENGTH);
    auto et = std::chrono::high_resolution_clock::now();

    int run = std::chrono::duration_cast<std::chrono::microseconds>(et-st).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
    }
  std::cout << std::endl;

  if(checkAnswer)
    {
      for(int index = 0; index < ARRAY_LENGTH; index++)
      {
      float t = az[index]/2000.0;
      if (t < 0.0)
        {
        t = 0.0;
        }
      else if (t > 1.0)
        {
        t = 1.0;
        }

      float correct = lerp(101325.0, 77325.0, t);

      if(fabs(correct-r[index]) > .01)
        {
        std::cerr << "Air Pressure wrong" << std::endl;
        std::cerr << " pressure at <" << ax[index] << ", " << ay[index] << ", " << az[index] << "> was ";
        std::cerr << r[index] << " but should be " << correct << std::endl;
        return 1;
        }
      }
    }

  if(saveTimes)
    {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(int index = 0; index < numIterations; index++)
      {
      outfile << times[index] << '\n';
      }
    }

  return 0;
}
