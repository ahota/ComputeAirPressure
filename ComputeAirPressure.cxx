//------------------------------------------------------------------------------
// simple worklet that uses the provided PointElevation worklet to compute
// the pressure at some arbitrary elevations
//
// this is to both re-familiarize myself with VTK-m and to test vectorization
// on any random worklet
//------------------------------------------------------------------------------

// TBB and CUDA versions will set this variable if they are present
// however if no other versions exist, we fall back to serial
#ifndef VTKM_DEVICE_ADAPTER
#define VTKM_DEVICE_ADAPTER VTKM_DEVICE_ADAPTER_SERIAL
#endif

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

#include <stdlib.h>
#include <string.h>

#include <vtkm/Math.h>
#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/PointElevation32.h>

bool writeToFile = false, saveTimes = false, checkAnswer = false;
vtkm::Id SIDE_LENGTH = 256;
std::string fileSuffix;

VTKM_CONT
vtkm::cont::ArrayHandle<vtkm::Float32>
ComputeAirPressure(
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3> > pointCoords,
    vtkm::Id numIterations)
{
  // this worklet will estimate the pressure at each point coordinate
  vtkm::worklet::PointElevation32 elevationWorklet;

  elevationWorklet.SetLowPoint(vtkm::Vec<vtkm::Float32, 3>(0.0f, 0.0f, 0.0f));
  elevationWorklet.SetHighPoint(vtkm::Vec<vtkm::Float32, 3>(0.0f, 0.0f, 2000.0f));
  elevationWorklet.SetRange(101325.0f, 77325.0f);

  // create a dispatcher for this worklet
  vtkm::worklet::DispatcherMapField<vtkm::worklet::PointElevation32>
    elevationDispatcher(elevationWorklet);

  // compute pressure!
  // timing code always takes up way more space than the real code >_>
  vtkm::cont::ArrayHandle<vtkm::Float32> pressure;
  vtkm::Id total = 0;
  vtkm::Id times[numIterations];
  std::cout << "Runs Single Average" << std::endl;
  for(vtkm::Id index = 0; index < numIterations; index++) {
    if(index > 0)
      std::cout << "\r";
    auto s = std::chrono::high_resolution_clock::now();

    // actually run the thing
    elevationDispatcher.Invoke(pointCoords, pressure);

    auto e = std::chrono::high_resolution_clock::now();
    vtkm::Id run =
      std::chrono::duration_cast<std::chrono::microseconds>(e-s).count();
    total += run;
    times[index] = run;
    std::cout << std::setw(4) << index+1 << " ";
    std::cout << std::setw(6) << run/1000.0 << " ";
    std::cout << std::setw(7) << total / (index+1) / 1000.0;
    std::cout << " ms   " << std::flush;
  }
  std::cout << std::endl;

  bool wrong = false;
  if(checkAnswer)
    {
    vtkm::cont::ArrayHandle<vtkm::Float32>::PortalControl pressurePortal = pressure.GetPortalControl();
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3> >::PortalControl
      portal = pointCoords.GetPortalControl();

    float lowz = 0.0f;
    float highz = 2000.0f;
    float rangeLow = 101325.0f;
    float rangeHigh = 77325.0f;
    float den = highz - lowz;
    int al = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;
    for(int i=0; i<al; i++)
      {
      float correct = ((rangeLow*(highz-portal.Get(i)[2])) + (rangeHigh*(portal.Get(i)[2]-lowz)))/den;
      if(fabs(correct-pressurePortal.Get(i)) > .01)
        {
        std::cerr << "Air Pressure Wrong.  At" << std::endl;
        std::cerr << "<" << portal.Get(i)[0] << ", " << portal.Get(i)[1] << ", " << portal.Get(i)[2] << "> should be ";
        std::cerr << correct << " but instead got " << pressurePortal.Get(i) << std::endl;
        wrong=true;
        break;
        }
      }
    }

  if(saveTimes && !wrong) {
    std::string filename = "times_" + std::to_string(SIDE_LENGTH) +
      "_" + fileSuffix + ".csv";
    std::ofstream outfile(filename);
    for(vtkm::Id index = 0; index < numIterations; index++) {
      outfile << times[index] << '\n';
    }
  }

  return pressure;
}

void printUsage()
{
  std::cerr << "Usage: ComputeAirPressure_SERIAL [-w ";
  std::cerr << "-n <numIterations> ";
  std::cerr << "-s ";
  std::cerr << "-l <length>]";
  std::cerr << std::endl;
}

int main(int argc, char **argv)
{
  int numIterations = 1;
  for(int argi = 1; argi < argc; argi++) {
    if(strcmp(argv[argi], "-w") == 0) {
      writeToFile = true;
    }
    else if (strcmp(argv[argi], "-n") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      numIterations = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-s") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      fileSuffix = argv[++argi];
      saveTimes = true;
    }
    else if (strcmp(argv[argi], "-l") == 0) {
      if(argi + 1 >= argc) {
        printUsage();
        return 1;
      }
      SIDE_LENGTH = atoi(argv[++argi]);
    }
    else if (strcmp(argv[argi], "-c") == 0) {
      checkAnswer = true;
    }
    else {
      std::cerr << "Unknown flag " << argv[argi] << std::endl;
      return 1;
    }
  }
  std::cout << "Compute Air Pressure" << std::endl;

  // array containing the elevation data that will be converted to pressure
  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3> > points;
  const vtkm::Id ARRAY_LENGTH = SIDE_LENGTH*SIDE_LENGTH*SIDE_LENGTH;
  points.Allocate(ARRAY_LENGTH);

  // populate the array with <x, y, elevation>
  vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::Float32, 3> >::PortalControl
    portal = points.GetPortalControl();
  for(vtkm::Id index = 0; index < ARRAY_LENGTH; index++) {
    portal.Set(index, vtkm::Vec<vtkm::Float32, 3>(
        index % SIDE_LENGTH,
        (index / SIDE_LENGTH) % SIDE_LENGTH,
        ((index / SIDE_LENGTH / SIDE_LENGTH) % SIDE_LENGTH) * (2000.0/float(SIDE_LENGTH))/*31.25*/));
    // multiplying by 31.25 moves values into the range 0-2000 (meters)
  }

  // calculate pressure from the elevations
  vtkm::cont::ArrayHandle<vtkm::Float32> pressure;
  pressure = ComputeAirPressure(points, numIterations);

  //remove below
  /*vtkm::cont::ArrayHandle<vtkm::Float32>::PortalControl pp = pressure.GetPortalControl();
  for(int i=0; i<ARRAY_LENGTH; i++)
    {
    std::cout << portal.Get(i) << '\t';
    std::cout << pp.Get(i) << std::endl;
    }*/

  if(writeToFile) {
    // builder to create a uniform grid
    vtkm::cont::DataSetBuilderUniform dataSetBuilder;
    vtkm::Id3 pointDims(SIDE_LENGTH, SIDE_LENGTH, SIDE_LENGTH);
    vtkm::Id3 cellDims = pointDims - vtkm::Id3(1, 1, 1);

    // create our grid as a tall column shape
    vtkm::cont::DataSet dataSet = dataSetBuilder.Create(
        pointDims, // number of points per dimension
        vtkm::Vec<vtkm::Float32, 3>(-32.0f, -32.0f, 0.0f), // center x&y dims
        vtkm::Vec<vtkm::Float32, 3>(1.0f, 1.0f, 4.0f)); // stretch z-axis
    // the "stretch" is just setting the spacing per axis

    // add to the dataset
    vtkm::cont::DataSetFieldAdd dataSetFieldAdd;
    dataSetFieldAdd.AddPointField(dataSet, "pressure", pressure);

    vtkm::io::writer::VTKDataSetWriter writer("pressure.vtk");
    writer.WriteDataSet(dataSet);
  }

  return 0;
}
